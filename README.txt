README for UPDI Plug
=====================

UPDI USB-serial programmer plug for Atmel MCUs (e.g. 0-series, some AtXMegas).


Design by: Konrad Rosenbaum
feel free to copy, change, sell, whatever...
Please remove the WI and Bug logos if you make significant changes.


Function
---------

This plug uses a CH340 chip to generate serial signals from USB, any OS with 
drivers will work. Linux/Android will work out of the box.

It uses a simple 1kΩ resistor to convert Rx/Tx to UPDI.

If you are using AVRdude use the "-c serialupdi" driver parameter.

The PCB contains two different plug types, one for 5V targets and one for 
3.3V targets. Use whichever one you need for your design.


Bill of Materials
------------------

The design uses the smallest CH340 package that can still be easily soldered.
All resistors are 0603 SMD types.
Capacitors are 0805 for 100nF, the polarized capacitor can be any type between 3-6mm SMD.

5V side:
* 1x CH340N chip SOP-8, 3.9x4.3mm size, 1.27mm pitch
* 1x 1kΩ 0603 for converting UART->UPDI
* 1x 100nF smoothing capacitor
* optional power LED:
 * 1x LED 0603 (any color)
 * 1x resistor 0603 (must reduce 5V to LED forward voltage, recommending 200-500Ω)
* optional 3pin header 2.54mm pitch

3.3V side:
* 1x CH340N chip SOP-8 3.9x4.3mm
* 1x AM1117-3.3V voltage regulator SOT223
* 1x 1kΩ 0603 for converting UART->UPDI
* 2x 100nF smoothing capacitor
* 1x 22µF polarized smoothing capacitor (any value between 10µF - 47µF will work fine)
* optional power LED:
 * 1x LED 0603 (any color)
 * 1x resistor 0603 (must reduce 5V to LED forward voltage, recommending 200-500Ω)
* optional 3pin header 2.54mm pitch

PCB Manufacture
----------------

The PCB is designed using a 2-layer layout with the bottom layer functioning as
ground plane.

If you want to switch to a 1-layer layout then the 3.3V version needs to be rerouted 
to connect the USB ground to the voltage regulator - e.g. by enlarging the two vias
and using a wire.

If you have a choice of thickness go for 2.4mm or 1.6mm thick PCBs.
 -> 2.4mm will fit the USB socket perfectly
 -> 1.6mm is easy to fix (see below)

If possible use ENIG (gold) finish - it makes the plugs more durable.
At the very least HASL (tin/nickel) finish is recommended.

Assembly
---------

All values and positions are indicated on the PCB, only the front side requires soldering.
All components are SMD (makes automated assembly cheaper) and at least 0603 (makes hand
assembly less fiddly).

Making the USB Plug fit:

The USB-A plug is integrated in the PCB, so the PCB thickness needs to fit the USB socket.
Most 2-layer PCBs have a thickness of 1.6mm, while USB-A connectors have a thickness of 
about 2.4mm at the connecting side.

This means that about 0.8mm need to be added to this part of the PCB. Luckily most
plastic cards (old credit cards, company badges, ski lift passes, google play vouchers, 
etc.) have a thickness of 0.75-0.85mm. Simply cut a 12mm square off an old card and glue 
it to the back of the plug. 

You may need to sand off a bit in case your card or PCB have different thickness - test 
it with a USB socket.


Freedom Units
--------------

Metric		US customary

0.8mm		31mil or 1/32in
1.6mm		63mil or 1/16in
2.4mm		94mil or 3/32in
2.54mm		1/10in
3mm		118mil or about 1/8in
6mm		236mil or about 1/4in
3.9mm		150mil
4.3mm		170mil
1.27mm		50mil

1608M		0603 (SMD)
2012M		0805 (SMD)

3.3V		2 2/8 AA batteries
5V		3 1/3 AA batteries

1kΩ		2.2 pound Ω
500Ω		about 1 pound Ω

100nF		3/16 tea spoons of electrons
22µF		1 3/64 lake Erie of electrons
